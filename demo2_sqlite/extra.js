// Logging

app.use(function (req, res, next) {
  console.log('GOT Request Time:%d  Url:%s', Date.now(), req.originalUrl);
  next();
});




// Sqlite


var sqlite  = require('sqlite3');
var db = new sqlite.Database('users.db');


app.get('/users', function(req, res) {
    db.all("select * from users", function(err, rows) {
        if(err){
            return res.send(err);
        }
        
        res.send(rows);
    });
 });


app.get('/users/:userid', function(req, res) {
    db.get("select * from users where id = " + req.params.userid, function(err, row) {
        if(err) {
            return res.send(err);
        }
        
        if(row) {
            res.send(row);
        }
        else{
            res.send('not found');
        }
    });
});